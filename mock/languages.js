Languages = [
	{ _id: 'html', title: 'HTML' },
	{ _id: 'css', title: 'CSS' },
	{ _id: 'javascript', title: 'JavaScript' },
	{ _id: 'php', title: 'PHP' },
	{ _id: 'sql', title: 'SQL' },
	{ _id: 'coffeescript', title: 'CoffeeScript' }
];