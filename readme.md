**Qometa - сервис для тестирования IT-специалистов**

Что умеет приложение?

- Позволяет создать любое количество вакансий
- В каждой вакансии может быть любое количество задач, по различным технологиям
- После добавление нового кандидата, приложение генерирует ссылку для прохождения теста
- Пройдя по ссылке, кандидат видит описание и может начать тестирование
- На каждый тест отводится определенное время
- Кандидат выполняет задание онлайн в удобном редакторе, с подсветкой синтаксиса
- Выполнение задания логируется, для того, чтобы можно было наблюдать ход размышлений кандидата
- За ходом выполнения задания можно наблюдать как в реальном времени, так и при помощи функции воспроизведения, после того как кандидат закончил выполнение
- После завершения тестирования кандидат попадает в раздел ожидания оценки, где вы можно оценить каждый из выполненных тестов
- После оценки всех тестов, которые прошел кандидат, он попадает в архив кандидатов
