import { Meteor } from 'meteor/meteor';

process.env.MAIL_URL = 'smtp://postmaster%40app.qometa.co:3f9e621bcb45fd299f93fd194cdf82d7@smtp.mailgun.org';

Meteor.startup(() =>
{
	Accounts.emailTemplates.siteName = "Qometa";
	Accounts.emailTemplates.from = "Awesome Qometa <hi@qometa.co>";

	Accounts.urls.resetPassword = function(token) {
		return Meteor.absoluteUrl('auth/reset/' + token);
	};

});