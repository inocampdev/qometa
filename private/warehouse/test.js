/**
 var d = new deferred();

 d.then(function(res){
	console.log("1 ", res);
	var d1 = new deferred();
	setTimeout(function() { d1.resolve("a"); }, 1000);
	return d1;
});
 d.then(function(res){ console.log("2 ", res); return "b"; });
 d.then(function(res){ console.log("3 ", res); return "c"; });
 d.resolve("hello");

 1  hello
 2  a
 3  b



function deferred()
{

	var queue = [];
	var index = 0;
	var onDone = function ()
	{
	};

	this.then = function (callback)
	{
		queue.push(callback);
	};

	this.done = function (callback)
	{
		onDone = callback;
	};

	this.resolve = function (res)
	{
		var next = function (index, res)
		{
			if (!queue[index])
			{
				onDone(res);
				return;
			}
			var nextRes = queue[index](res);
			nextRes.done ? nextRes.done(function (res)
			{
				next(index + 1, res);
			}) : next(index + 1, nextRes);

		};

		next(0, res);
	};

}

var d = new deferred();

d.then(function (res)
{
	console.log("1 ", res);
	var d1 = new deferred();
	setTimeout(function ()
	{
		d1.resolve("a");
	}, 1000);
	return d1;
});
d.then(function (res)
{
	console.log("2 ", res);
	return "b";
});
d.then(function (res)
{
	console.log("3 ", res);
	return "c";
});
d.resolve("hello");

 */