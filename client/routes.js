FlowRouter.route('/', {
	action(params)
	{
		FlowRouter.go(Meteor.userId() ? '/quests/jobs' : '/auth/login');
	}
});
