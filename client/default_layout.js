Template.default_layout.onCreated(function ()
{
	if (!Meteor.userId())
	{
		FlowRouter.go('/auth/login');
	}
	Quests.subscribe(this);
});