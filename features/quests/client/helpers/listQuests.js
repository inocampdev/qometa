Template.registerHelper('quests_listQuests', function(_job)
{
	_job = _job || FlowRouter.getParam('_job');
	return QuestsJobsQuests.find({ _job: _job });
});