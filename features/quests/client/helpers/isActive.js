Template.registerHelper('quests_isActive', function(key, _id)
{
	return FlowRouter.getParam(key) === _id;
});