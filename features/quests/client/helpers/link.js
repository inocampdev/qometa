Template.registerHelper('quests_link', (subPage, params) =>
{
	let link = params.hash.absolute ? Meteor.absoluteUrl() + `quests/${subPage}` : `/quests/${subPage}`;

	if (params.hash._candidate) link += `/${params.hash._candidate}`;
	if (params.hash._job) link += `/${params.hash._job}`;
	if (params.hash._quest) link += `/${params.hash._quest}`;
	if (params.hash.mod) link += `/${params.hash.mod}`;

	return link;
});