Template.registerHelper('quests_activeCandidate', () =>
{
	return QuestsCandidates.findOne(FlowRouter.getParam('_candidate'));
});