Session.set('quests_listCandidates_activeNow', null);

Template.registerHelper('quests_listCandidates', function(_job, status)
{
	_job = _job || FlowRouter.getParam('_job');
	let query = { };
	if (_job)
	{
		query._job = _job;
	}
	if (_.isString(status) || FlowRouter.getParam('status'))
	{
		query.status = status || FlowRouter.getParam('status')
	}

	if (Session.get('quests_listCandidates_activeNow'))
	{
		query = {
			$or: [
					query, { _id: Session.get('quests_listCandidates_activeNow') }
			]
		}
	}

	return QuestsCandidates.find(query);
});