Template.quests_quests_form.helpers({
	onSubmit()
	{
		return (data, formTemplate, formElement) =>
		{
			if (!data._job)
			{
				data._job = FlowRouter.getParam('_job');
			}
			_CRUD.Forms.upsert(QuestsJobsQuests, (err, res) =>
			{
				if (_defaultMethodCallback(err, res))
				{
					Template.modal.hide();
				}
			})(data, formTemplate, formElement);
		}
	},
	languages()
	{
		return Languages;
	}
});