Template.quests_interviews_page.onCreated(function ()
{
	this.autorun(() =>
	{
		if (FlowRouter.getParam('_candidate') && FlowRouter.getParam('_quest'))
		{
			this.subscribe('quests_candidates_tracks', {
				_candidate: FlowRouter.getParam('_candidate'),
				_quest: FlowRouter.getParam('_quest')
			});
		}
	});
});

Template.quests_interviews_page.helpers({
	onSubmitCandidate()
	{
		return (data, formTemplate, formElement) =>
		{
			data.status = 'pending';
			data.startUTS =  0;
			data.interview = {};
			_CRUD.Forms.upsert(QuestsCandidates, (err, res) =>
			{
				if (_defaultMethodCallback(err, res))
				{
					Template.form.tools.clearForm(formElement);
					Template.modal.hide();
				}

			})(data, formTemplate);
		}
	},
	candidates()
	{
		return QuestsCandidates.find();
	},
	activeCandidate()
	{
		return QuestsCandidates.findOne(FlowRouter.getParam('_candidate'));
	},
	activeCandidateQuests()
	{
		let candidate = QuestsCandidates.findOne(FlowRouter.getParam('_candidate'));
		if (!candidate || !candidate.interview.quests)
		{
			return;
		}
		return _.values(candidate.interview.quests);
	},
	activeQuest()
	{
		let candidate = QuestsCandidates.findOne(FlowRouter.getParam('_candidate'));
		if (!candidate || !candidate.interview.quests)
		{
			return;
		}
		return candidate.interview.quests[FlowRouter.getParam('_quest')];
	},
	activeQuestTracks()
	{
		if (FlowRouter.getParam('_candidate') && FlowRouter.getParam('_quest'))
		{
			return QuestsCandidatesTracks.find({
				_candidate: FlowRouter.getParam('_candidate'),
				_quest: FlowRouter.getParam('_quest')
			});
		}
	}
});