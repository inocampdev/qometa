FlowRouter.route('/quests/jobs', {
	action(params) {
		Pages.show('quests_admin_page', params);
	}
});

FlowRouter.route('/quests/jobs/:_job', {
	action(params) {
		Pages.show('quests_job_page', params);
	}
});

FlowRouter.route('/quests/jobs/:_job/quests', {
	action(params) {
		Pages.show('quests_job_quests_page', params);
	}
});

FlowRouter.route('/quests/jobs/:_job/candidates/:status?', {
	action(params) {
		Session.set('quests_listCandidates_activeNow', null);
		Pages.show('quests_job_candidates_page', params);
	}
});

FlowRouter.route('/quests/candidates', {
	action(params) {
		FlowRouter.go('/quests/candidates/pending');
	}
});

FlowRouter.route('/quests/candidates/:status/', {
	action(params) {
		Session.set('quests_listCandidates_activeNow', null);
		Pages.show('quests_candidates_page', params);
	}
});

FlowRouter.route('/quests/candidates/:status/:_candidate', {
	action(params) {
		Pages.show('quests_candidates_details_page', params);
	}
});

FlowRouter.route('/quests/candidate/thankyou', {
	action(params) {
		Pages.show('quests_candidate_thankyou_page', params, 'candidate_layout');
	}
});

FlowRouter.route('/quests/candidate/:_candidate/:_job', {
	action(params) {
		Pages.show('quests_candidate_page', params, 'candidate_layout');
	}
});

FlowRouter.route('/quests/candidate/:_candidate/:_job/interview/:_quest?', {
	action(params) {
		Pages.show('quests_candidate_interview_page', params, 'candidate_layout');
	}
});

FlowRouter.route('/quests/settings', {
	action(params) {
		Pages.show('quests_settings_page', params);
	}
});

