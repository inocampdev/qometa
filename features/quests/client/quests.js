Quests = {
	subscribe(templateInstance)
	{
		templateInstance.autorun(() =>
		{
			if (!Meteor.userId())
			{
				return;
			}

			templateInstance.subscribe('quests_jobs');

			if (FlowRouter.getParam('_job'))
			{
				templateInstance.subscribe('quests_jobs_quests', { _job: FlowRouter.getParam('_job') });
				templateInstance.subscribe('quests_candidates', { _job: FlowRouter.getParam('_job') });
			}

			if (FlowRouter.getParam('_quest'))
			{
				templateInstance.subscribe('quests_jobs_quests', FlowRouter.getParam('_quest'));
			}

			if (FlowRouter.getParam('_candidate'))
			{
				templateInstance.subscribe('quests_candidates', FlowRouter.getParam('_candidate'));
			}

			if (FlowRouter.getParam('status'))
			{
				templateInstance.subscribe('quests_candidates', { status: FlowRouter.getParam('status') });
			}

		});

	}

};