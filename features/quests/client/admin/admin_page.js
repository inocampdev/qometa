Template.quests_admin_page.onCreated(function()
{ });

Template.quests_admin_page.onRendered(function()
{ });

Template.quests_admin_page.helpers({

	link(params)
	{
		let link = '/quests/admin';
		if (params.hash._job) link += `/${params.hash._job}`;
		if (params.hash._quest) link += `/${params.hash._quest}`;
		return link;
	},

	jobs()
	{
		return QuestsJobs.find();
	},

	activeJob()
	{
		return QuestsJobs.findOne(FlowRouter.getParam('_job'))
	},

	quests()
	{
		return QuestsJobsQuests.find({
			_job: FlowRouter.getParam('_job')
		});
	},

	activeQuest()
	{
		return QuestsJobsQuests.findOne(FlowRouter.getParam('_quest'));
	}
});
