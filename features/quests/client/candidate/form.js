Template.quests_candidate_form.helpers({
	onSubmit()
	{
		return (data, formTemplate, formElement) =>
		{
			data.status = 'pending';
			data.interviewStartedAtUTS =  0;
			data.interview = {};
			data._job = FlowRouter.getParam('_job');
			_CRUD.Forms.upsert(QuestsCandidates, (err, res) =>
			{
				if (_defaultMethodCallback(err, res))
				{
					Template.form.tools.clearForm(formElement);
					Template.modal.hide();
				}

			})(data, formTemplate);
		}
	},
	jobs()
	{
		return QuestsJobs.find();
	}
});