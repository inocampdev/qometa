Template.quests_candidate_interview_page.onCreated(function ()
{
	this.timeLeft = new ReactiveVar({
		minutes: 0,
		seconds: 0
	});

	this.startedQuest = null;

	this.candidate = () =>
	{
		return QuestsCandidates.findOne(FlowRouter.getParam('_candidate'))
	};

	this.autorun(() =>
	{
		let candidate = this.candidate();
		let candidateQuests = candidate.interview.quests;
		if (candidateQuests)
		{
			let nextQuest = _.findWhere(candidateQuests, {status: 'started'}) || _.findWhere(candidateQuests, {status: 'pending'});

			if (!nextQuest)
			{
				FlowRouter.go('/quests/candidate/thankyou');
				return;
			}

			this.startedQuest = nextQuest;

			if (nextQuest && FlowRouter.getParam('_quest') != nextQuest._id)
			{
				if (nextQuest.status == 'started')
				{
					FlowRouter.go(`/quests/candidate/${candidate._id}/${candidate._job}/interview/${nextQuest._id}`);
				}
				else
				{
					Meteor.call('quests_quest_start', candidate._id, nextQuest._id, (err, res) =>
					{
						if (_defaultMethodCallback(err, res))
						{
							this.startedQuest = nextQuest;
							FlowRouter.go(`/quests/candidate/${candidate._id}/${candidate._job}/interview/${nextQuest._id}`);
						}
					});
				}
			}
		}
	});

	setInterval(() =>
	{
		if (this.startedQuest)
		{
			let timeLeft = (this.startedQuest.expiresAtUTS - Date.now()) / 1000;
			this.timeLeft.set({
				minutes: timeLeft / 60 | 0,
				seconds: timeLeft % 60 | 0
			});
		}
	}, 1000);

});

Template.quests_candidate_interview_page.helpers({
	startedQuestInfo()
	{
		let quests = QuestsJobsQuests.find({_job: FlowRouter.getParam('_job')}).fetch();
		let result = {
			count: quests.length,
			index: quests.findIndex((quest, index) => quest._id == FlowRouter.getParam('_quest')) + 1
		};
		return result;
	},
	timeLeft()
	{
		return Template.instance().timeLeft.get();
	},
	startedQuest()
	{
		let startedQuest = Template.instance().candidate().interview.quests[FlowRouter.getParam('_quest')];
		return startedQuest;
	}
});

Template.quests_candidate_interview_page.events({
	'click ._finishQuest'(e, t)
	{
		Meteor.call('quests_quest_finish', FlowRouter.getParam('_candidate'), t.startedQuest._id);
	}
});