

Template.quests_candidate_page.helpers({
	welcomeInfo()
	{
		let quests = QuestsJobsQuests.find({_job: FlowRouter.getParam('_job')}).fetch();
		return {
			count: quests.length,
			duration: _.reduce(quests, (memo, quest) =>
			{
				return memo + quest.minutes;
			}, 0)
		}
	}
});

Template.quests_candidate_page.events({
	'click ._start'()
	{
		Meteor.call('quests_interview_start', FlowRouter.getParam('_candidate'), (err, res) =>
		{
			if (_defaultMethodCallback(err, res))
			{
				FlowRouter.go(`/quests/candidate/${FlowRouter.getParam('_candidate')}/${FlowRouter.getParam('_job')}/interview`);
			}
		});
	}
});