Session.set('quests_player_quest', null);

Template.quests_player.onCreated(function ()
{
	this.activeQuest = new ReactiveVar(_.values(this.data.candidate.interview.quests)[0]);
	this.playerInterval = null;
	this.sequence = null;

	this.subscribe('quests_candidates', this.data.candidate._id);
});

Template.quests_player.helpers({
	quest()
	{
		return Template.instance().activeQuest.get();
	},

	scores()
	{
		return Scores;
	},

	interviewScore()
	{
		return this.candidate.interview.score;
	},

	interviewMaxScore()
	{
		return this.candidate.interview.maxScore;
	},

	onBadgeClick()
	{
		let instance = Template.instance();
		return (badgeInstance) =>
		{
			//Session.set('quests_player_quest', badgeInstance.data.quest);
			instance.activeQuest.set(badgeInstance.data.quest);
		}
	}

});

Template.quests_player.onRendered(function ()
{

	this.autorun(() =>
	{

		if (!this.editor)
		{
			Tracker.afterFlush(() =>
			{
				this.editor = CodeMirror.fromTextArea(this.$('textarea')[0], {
					lineNumbers: true
				});
			});
		}

		let query = {
			_candidate: this.data.candidate._id,
			_quest: this.activeQuest.get()._id
		};

		this.subscribe('quests_candidates_tracks', query, ()=>
		{

			let recentTrack = QuestsCandidatesTracks.find(query, {sort: {createdAt: -1}});

			Tracker.afterFlush(() =>
			{
				if (recentTrack.fetch()[0])
				{
					this.editor.setValue(recentTrack.fetch()[0].state);
					setTimeout(() =>
					{
						this.editor.refresh();
					},1);
				}
				else
				{
					this.editor.setValue('');
				}

			});

		});

	});

});

Template.quests_player.events({
	'click'(e, t)
	{
		Session.set('quests_listCandidates_activeNow', t.data.candidate._id);
	},
	'change'(e, t)
	{
		Meteor.call(
			'quests_quest_rate', t.data.candidate._id, t.activeQuest.get()._id, +$(e.currentTarget).val(),
			_defaultMethodCallback
		)
	},
	'click ._play'(e, t)
	{
		clearInterval(t.playerInterval);
		let query = {_candidate: t.data.candidate._id, _quest: t.activeQuest.get()._id};

		t.sequence = QuestsCandidatesTracks.find(query, {sort: {createdAt: 1}}).fetch();

		setInterval(() =>
		{
			let track = t.sequence.shift();

			if (track)
			{
				t.editor.setValue(track.state);
			}
			else
			{
				clearInterval(t.playerInterval);
			}

		}, 1000);

	}

});
