Meteor.methods({
	'quests_interview_start'(_candidate)
	{
		check(_candidate, String);

		let candidate = QuestsCandidates.findOne(_candidate);
		check(candidate, Object);

		let quests = _.indexBy(QuestsJobsQuests.find({_job: candidate._job}, {sort: {createdAt: -1}}).fetch(), '_id');
		let job = QuestsJobs.findOne(candidate._job);

		let patch = {
			status: 'startedInterview',
			interview: {}
		};

		_.each(quests, (quest) =>
		{
			quest.status = 'pending';
			quest.startedAtUTS = 0;
			quest.score = -1;
		});

		patch.interview = {
			score: 0,
			maxScore: _.size(quests) * 3, // максимальное количество балов которе можно набрать за тест
			job: job,
			quests: quests
		};

		QuestsCandidates.update(_candidate, {$set: patch});
	},
	'quests_quest_start'(_candidate, _quest)
	{
		check(_candidate, String);
		check(_quest, String);

		let candidate = QuestsCandidates.findOne(_candidate);
		let quest = candidate.interview.quests[_quest];
		let patch = {};

		if (quest.startedAtlUTS > 0)
		{
			throw new Meteor.Error('test already started');
		}

		patch[`interview.quests.${_quest}.status`] = 'started';
		patch[`interview.quests.${_quest}.startedAtUTS`] = Date.now();
		patch[`interview.quests.${_quest}.expiresAtUTS`] = Date.now() + quest.minutes * 60 * 1000;

		QuestsCandidates.update(_candidate, {$set: patch});
	},
	'quests_quest_finish'(_candidate, _quest)
	{
		let candidate = QuestsCandidates.findOne(_candidate);
		let patch = { };
		let interviewFinished = true;

		candidate.interview.quests[_quest].status = 'finished';

		_.each(candidate.interview.quests, (quest) =>
		{
			if (quest.status != 'finished')
			{
				interviewFinished = false;
			}
		});

		patch[`interview.quests.${_quest}.status`] = candidate.interview.quests[_quest].status;

		if (interviewFinished)
		{
			patch['status'] = 'finishedInterview';
		}

		QuestsCandidates.update(_candidate, {$set: patch});

	},

	'quests_quest_rate'(_candidate, _quest, _score)
	{
		let candidate = QuestsCandidates.findOne(_candidate);
		let patch = { };
		let interviewScore = 0;
		let reviewFinished = true;

		candidate.interview.quests[_quest].score = _score;

		_.each(candidate.interview.quests, (quest) =>
		{
			if (quest.score < 0)
			{
				reviewFinished = false;
				return;
			}
			interviewScore += quest.score;
		});

		patch[`interview.quests.${_quest}.score`] = _score;
		patch['interview.score'] = interviewScore;

		if (reviewFinished)
		{
			patch['status'] = 'reviewFinished';
		}
		else
		{
			patch['status'] = 'finishedInterview';
		}

		QuestsCandidates.update(_candidate, {$set: patch});

	}

});