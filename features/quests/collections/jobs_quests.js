

QuestsJobsQuests = new Mongo.Collection('quests_jobs_quests');

QuestsJobsQuests.attachSchema(new SimpleSchema({
	_user: {
		type: String,
		autoValue()
		{
			return Meteor.userId()
		}
	},
	_job: {
		type: String
	},
	createdAt: {
		type: Date
	},
	updatedAt: {
		type: Date
	},
	title: {
		type: String
	},
	lang: {
		type: String
	},
	minutes: {
		type: Number
	}
}));

Meteor.methods(_CRUD.methods(QuestsJobsQuests));
_publish(QuestsJobsQuests);