/**
 *
 * /**
 "_id" : "pQwTY2mFPvnpwvDw6",
 "name" : "Сергей",
 "email" : "sergey@gmail.com",
 "status" : "finishedInterview",
 "interviewStartedAtUTS" : 0,
 "interview" : {

		}
 },
 "_job" : "tr4d6EXk96CWNi6W2",
 "createdAt" : ISODate("2016-09-13T12:55:12.614Z"),
 "updatedAt" : ISODate("2016-09-13T12:55:12.614Z")
 }

 */

QuestsCandidates = new Mongo.Collection('quests_candidates');

QuestsCandidates.attachSchema(new SimpleSchema({
	_user: {
		type: String,
		optional: true,
		autoValue()
		{
			if (!this.isUpdate)
			{
				return Meteor.userId()
			}
		}
	},
	_job: {
		type: String
	},
	createdAt: {
		type: Date
	},
	updatedAt: {
		type: Date
	},
	name: {
		type: String
	},
	email: {
		type: String
	},
	status: {
		type: String
	},
	interviewStartedAtUTS: {
		type: Number
	},
	interview: {
		type: Object,
		blackbox: true
	}
}));

Meteor.methods(_CRUD.methods(QuestsCandidates));
_publish(QuestsCandidates);