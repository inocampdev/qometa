QuestsJobs = new Mongo.Collection('quests_jobs');

QuestsJobs.attachSchema(new SimpleSchema({
	_user: {
		type: String,
		autoValue()
		{
			return Meteor.userId()
		}
	},
	createdAt: {
		type: Date
	},
	updatedAt: {
		type: Date
	},
	title: {
		type: String
	},
	description: {
		type: String
	}
}));

Meteor.methods(_CRUD.methods(QuestsJobs));
_publish(QuestsJobs);