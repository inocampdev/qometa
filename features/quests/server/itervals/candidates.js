
Meteor.setInterval(() =>
{
	let candidates = QuestsCandidates.find({ status: 'startedInterview' }).fetch();
	_.each(candidates, (candidate) =>
	{

		let questStatusChanged = false;
		let interviewFinished = true;

		_.each(candidate.interview.quests, (quest) =>
		{
			if (quest.status != 'finished' && quest.expiresAtUTS < Date.now())
			{
				quest.status = 'finished';
				questStatusChanged = true;
			}
			if (quest.status != 'finished')
			{
				interviewFinished = false;
			}
		});

		if (interviewFinished)
		{
			candidate.status = 'finishedInterview';
		}

		if (questStatusChanged || interviewFinished)
		{
			Meteor.call('quests_candidates_update', candidate._id, {
				status: candidate.status,
				interview: candidate.interview
			});
		}

	});

}, 2000);
