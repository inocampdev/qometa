Pages = {
	_page: new ReactiveVar(null),
	_layout: new ReactiveVar(null),
	show(template, data, layout = 'default_layout')
	{

		BlazeLayout.render(layout, {
			template: template
		});

	}
};