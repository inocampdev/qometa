Template.pages_drawer.helpers({
	page()
	{
		return Pages._page.get();
	},

	layout()
	{
		return Pages._layout.get();
	}

});