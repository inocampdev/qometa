Template.auth_pages_register.helpers({
	onSubmit()
	{
		return (user, form, element) =>
		{
			check(user, {
				email: String,
				password: String,
				profile: {
					name: String
				}
			});
			Accounts.createUser(user, (err, res)=>
			{
				if (!_defaultMethodCallback(err, res))
				{
					form.error.set(err.reason);
				}
				else
				{
					FlowRouter.go('/quests/jobs');
				}
			});
		}
	}
});