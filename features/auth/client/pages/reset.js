Template.auth_pages_reset.helpers({
	onSubmit()
	{
		return (data, form, element)=>
		{
			Accounts.resetPassword(FlowRouter.getParam('token'), data.password, _defaultMethodCallback);
		}
	}
});