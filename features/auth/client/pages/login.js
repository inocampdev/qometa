Template.auth_pages_login.helpers({
	onSubmit()
	{
		return (user, formInstance, formElement)=>
		{
			Meteor.loginWithPassword(user.email, user.password, (err, res)=>
			{
				if (!_defaultMethodCallback(err, res))
				{
					formInstance.error.set(err.reason);
				}
				else
				{
					FlowRouter.go('/quests/jobs');
				}
			});
		}
	}
});