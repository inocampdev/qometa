Template.auth_pages_forgot.helpers({
	onSubmit()
	{
		return (data, form, element) =>
		{
			Accounts.forgotPassword(data, (err, res) =>
			{
				if (_defaultMethodCallback(err, res))
				{
					alert('Ссылка для восстановления пароля отправлена на вашу почту');
				}
			});
		}
	}
});