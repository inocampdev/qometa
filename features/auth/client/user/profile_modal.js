Template.auth_user_profile_modal.helpers({
	onSubmit()
	{
		return (data, form, template) =>
		{
			var patch = {
				'profile.name': data.name
			};
			Meteor.call('users_update', Meteor.userId(), patch, (err, res)=>
			{
				if (_defaultMethodCallback(err, res) )
				{
					Template.modal.hide();
				}
			});
		}
	}
});

Template.auth_user_profile_modal.events({
	'click ._logout'(e, t)
	{
		Meteor.logout((err) =>
		{
			FlowRouter.go('/');
		});
	}
});