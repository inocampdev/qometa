FlowRouter.route('/auth/:page/:token?', {
	action(params)
	{
		//Pages.show(`auth_pages_${params.page}`, params, 'auth_layouts_default');
		BlazeLayout.render('auth_layouts_default', {
			template: `auth_pages_${params.page}`
		})
	}
});
