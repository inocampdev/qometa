_CRUD = {
	methods(collection)
	{
		let methods = {};
		methods[collection._name + '_insert'] = this.insert(collection);
		methods[collection._name + '_update'] = this.update(collection);
		methods[collection._name + '_remove'] = this.remove(collection);

		//console.log('methods', _.keys(methods));

		return methods;
	},

	insert(collection)
	{
		return function (data)
		{
			data.createdAt = new Date;
			data.updatedAt = new Date;
			data._id = collection.insert(data);
			return data;
		}
	},

	update(collection)
	{
		return function (_id, data)
		{
			data.updatedAt = new Date;
			collection.update(_id, {$set: data});
			return data;
		}
	},

	remove(collection)
	{
		return function(_id)
		{
			return collection.remove(_id);
		}
	},

	Forms: {
		upsert(collection, callback)
		{
			callback = callback || _defaultMethodCallback;

			return (data, formTemplate, formElement) =>
			{
				if (formTemplate.data.doc)
				{
					Meteor.call(collection._name + '_update', formTemplate.data.doc._id, data, callback);
				}
				else
				{
					Meteor.call(collection._name + '_insert', data, callback);
					Template.form.tools.clearForm(formElement);
				}
			}

		}

	}

};