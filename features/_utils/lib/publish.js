_publish = (collection) =>
{
	if (Meteor.isServer)
	{
		console.log('_publish', collection._name);
		Meteor.publish(collection._name, function(query, options)
		{
			options = options || { sort: { createdAt: -1 } };
			query = query || {};

			if (!_.isString(query)) query._user = this.userId;

			console.log('_publish', collection._name, query);
			return collection.find(query, options);
		});
	}
};