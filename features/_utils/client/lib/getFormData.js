_getFormData = function(e) {

	console.warn('_getFormData', 'using get form data is deprecated. use Template.form.tools.getFormData instead');
	var data = {};
	var $currentTarget = $(e.currentTarget);
	var fields = $currentTarget.find('[name]');

	_.each(fields, (field) => {
		var $field = $(field);
		var val = $field.val().trim();
		data[$field.attr('name')] = val.match(/^[0-9]+$/) ? +val : val;
	});

	return data;

};