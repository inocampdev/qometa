Template._delete.events({
	'click'(e, t)
	{
		e.preventDefault();
		if (confirm('Удалить эту запись?'))
		{
			console.log(t.data.collection + '_remove');
			Meteor.call(t.data.collection + '_remove', t.data._id, _defaultMethodCallback);
		}
	}
});