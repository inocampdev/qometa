Template.registerHelper('_id', (params) =>
{
	let id = [];

	params.hash.action = params.hash.action || 'view';

	if (params.hash.entity) id.push(params.hash.entity);
	if (params.hash.action) id.push(params.hash.action);
	if (params.hash._id) id.push(params.hash._id);

	return id.join('_');
});