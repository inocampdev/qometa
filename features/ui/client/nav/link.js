Template.nav_link.helpers({
	activeClass()
	{
		let path = FlowRouter.current().path;
		let cond = (!this.direct && path.indexOf(this.href) != -1) || (this.direct && path === this.href);
		return cond ? '__active' : '';
	}
});