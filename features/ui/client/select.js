Template.select.helpers({
	title(option)
	{
		return option.title || option.name;
	},
	selectedAttr(option)
	{
		return option._id == this.value
				? { selected: 'selected' }
				: null;
	}
});