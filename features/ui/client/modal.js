Template.modal.helpers({
	activeClass()
	{
		return Session.get('modal') == this.cid
			? 'modal__active'
			: '';
	}
});

Template.modal.events({
	'click .modal_content'(e, t)
	{
		e.stopPropagation();
	},
	'click .modal'(e, t)
	{
		Template.modal.hide();
	},
	'click ._close'(e, t)
	{
		e.preventDefault();
		e.stopPropagation();
		Template.modal.hide();
	}
});

Template.modal.show = (cid) =>
{
	Session.set('modal', cid);
};

Template.modal.hide = () =>
{
	Session.set('modal', null);
};