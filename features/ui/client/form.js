Template.form.onCreated(function()
{
	this.error = new ReactiveVar(null);
});

Template.form.onRendered(function ()
{
	let self = this;
	if (this.data.doc)
	{
		this.$('[name]').each(function ()
		{
			let $field = $(this);
			$field.val(self.data.doc[this.name]);
		});
	}
});

Template.form.helpers({
	error()
	{
		return Template.instance().error.get();
	}
});

Template.form.events({
	'click .input'(e, t)
	{
		$(e.currentTarget).removeClass('input__invalid');
	},
	'submit'(e, t)
	{
		e.preventDefault();
		let isValid = true;
		let requiredValidation = t.$('[data-regex]');
		requiredValidation.removeClass('input__invalid');
		requiredValidation.each(function ()
		{
			let $input = $(this);
			let pattern = $input.data('regex');
			let matchesPattern = $input.val().match(new RegExp(pattern, 'g'));
			if (!matchesPattern)
			{
				$input.addClass('input__invalid');
				isValid = false;
			}
		});
		if (isValid && t.data.onSubmit)
		{
			t.data.onSubmit.call(null, Template.form.tools.getFormData(e.currentTarget), t, e.currentTarget);
			if (t.data.clearAfterSubmit === true)
			{
				Template.form.tools.clearForm(e.currentTarget);
			}
		}
	}
});

Template.form.tools = {
	clearForm(formEl)
	{
		$(formEl).find('[name]').val('');
	},
	getFormData(formEl, parseDotNotation = true)
	{
		var data = {};
		var $currentTarget = $(formEl);
		var fields = $currentTarget.find('[name]');
		_.each(fields, (field) =>
		{
			var $field = $(field);
			var val = $field.val().trim();
			var name = $field.attr('name');
			if (name.match(/\.+/) && parseDotNotation)
			{
				var tokens = name.split('.');
				var memo = data;
				_.each(tokens, (token,  index) =>
				{
					memo[token] = memo[token] || tokens.length - 1 === index ? val : {};
					memo = memo[token];
				});
			}
			else
			{
				data[name] = val.match(/^[0-9]+$/) ? +val : val;
			}
		});
		return data;
	}
};