Template.nav.onCreated(function()
{
	this.links = [];
});
Template.nav.onRendered(function()
{
	let self = this;
	let links = this.$('a');

	links.each(function()
	{
		self.links.push($(this).attr('href'));
	});
	this.links.sort(function(a, b)
	{
		return a.length < b.length;
	});
	this.autorun(() =>
	{
		FlowRouter.watchPathChange();
		links.removeClass('__active');
		_.each(this.links, (link) =>
		{
			if (FlowRouter.current().path.indexOf(link) != -1)
			{
				this.$(`[href="${link}"]`).addClass('__active');
			}
		});
	});
});