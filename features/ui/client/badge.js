Session.set('badge', null);

Template.badge.helpers({
	hrefAttr()
	{
		return this.link
			? {href: this.link}
			: {}
	},
	isActiveClass()
	{
		return this.isActive
			? '__active'
			: '';
	}
});

Template.badge.events({
	'click'(e, t)
	{
		if (t.data.onClick)
		{
			t.data.onClick.call(null, t);
		}
	}
});