Template.editor.onRendered(function()
{
	let editor = CodeMirror.fromTextArea(this.$('textarea')[0], {
		lineNumbers: true
	});

	editor.on('change', _.debounce((doc, t) =>
	{
		Meteor.call('quests_candidates_tracks_insert', {
			_user: this.data.candidate._user,
			_candidate: this.data.candidate._id,
			_quest: this.data.quest._id,
			state: doc.getValue()
		}, _defaultMethodCallback);
	}, 1000));

});