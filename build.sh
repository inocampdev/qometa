#!/usr/bin/env bash

if [ ! -d /usr/local/meteor ]; then
    mkdir -m 755 /usr/local/meteor
fi

if [ -d /usr/local/meteor/qometa ]; then
    rm -rf /usr/local/meteor/qometa
fi


mkdir -m 755 /usr/local/meteor/qometa

npm install --production
meteor build /usr/local/meteor/qometa --architecture os.linux.x86_64
tar -xvzf /usr/local/meteor/qometa/qometa.tar.gz -C /usr/local/meteor/qometa

(cd /usr/local/meteor/qometa/bundle/programs/server && npm install)

export MONGO_URL='mongodb://localhost/qometa'
export ROOT_URL='http://app.qometa.co'
export PORT=80

cd /usr/local/meteor/qometa/bundle
node main.js
